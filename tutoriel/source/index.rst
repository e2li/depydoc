depydoc
=======

Introduction
------------

Le projet depydoc est un long tutoriel.

* En Python
* En Français
* Partant de zéro


.. toctree::
   :maxdepth: 1
   :caption: Table des matières:

   ./01-sockets.rst
   ./02-http.rst
   ./03-serveurs.rst
   ./04-frameworks.rst
   ./05-flask.rst
   ./06-html.rst
