from flask import Flask
import os

app = Flask("depydoc")
app.config["RÉPERTOIRE_DE_BASE"] = "test/docs"


@app.route("/")
def sert_racine():
    print("sert_racine")
    return sert_chemin("/")


@app.route("/<path:chemin>")
def sert_chemin(chemin):
    print("sert_chemin")
    répertoire_de_base = app.config["RÉPERTOIRE_DE_BASE"]
    racine = répertoire_de_base + chemin
    if not os.path.exists(racine):
        return "chemin non trouvé", 404

    return "Contenu de " + chemin
