from http import HTTPStatus
from http.server import BaseHTTPRequestHandler, HTTPServer
from socketserver import ThreadingMixIn
import sys
import os


IP = "127.0.0.1"
PORT = 8080


def lire_répertoire(racine):
    entrées = os.listdir(racine)
    entrées.sort()

    return entrées


def traiter_entrées(racine, entrées):
    liste_traitée = []

    for entrée in entrées:
        if entrée.startswith("."):
            # Le fichier est caché
            continue
        if os.path.isdir(racine + entrée):
            liste_traitée.append(entrée + "/")
        else:
            liste_traitée.append(entrée)

    return liste_traitée


def formater_liste(liste):
    listing = ""
    for élément in liste:
        listing += élément
        listing += "\n"
    return listing


class DepydocRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == "/favicon.ico":
            return self.sert_icône()
        else:
            return self.sert_chemin()

    def sert_chemin(self):
        répertoire_de_base = self.server.répertoire_de_base

        racine = répertoire_de_base + self.path
        if not os.path.exists(racine):
            self.send_error(
                HTTPStatus.NOT_FOUND, message="Not Found", explain="Chemin non trouvé"
            )
            return

        contenu_du_répertoire = lire_répertoire(racine)
        contenu_traité = traiter_entrées(racine, contenu_du_répertoire)
        liste_formatée = formater_liste(contenu_traité)
        titre = "Contenu de " + self.path + "\n"
        réponse = titre + liste_formatée

        encodé = réponse.encode()
        self.envoie_réponse(encodé, "text/plain")

    def sert_icône(self):
        with open("../../favicon.ico", "rb") as f:
            data = f.read()
        self.envoie_réponse(data, "img/x-icon")

    def envoie_réponse(self, data, content_type):
        self.send_response(HTTPStatus.OK, "OK")
        self.send_header("Content-type", content_type)
        self.send_header("Content-Length", str(len(data)))
        self.end_headers()
        self.wfile.write(data)


class DepydocServeur(HTTPServer, ThreadingMixIn):
    pass


def main():
    path = sys.argv[1]
    serveur = DepydocServeur((IP, PORT), DepydocRequestHandler)
    serveur.répertoire_de_base = path
    print("J'écoute sur ", "http://" + IP + ":" + str(PORT))
    serveur.serve_forever()


if __name__ == "__main__":
    main()
